<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $jogo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Apagar esse jogo'),
                ['action' => 'delete', $jogo->id],
                ['confirm' => __('Você deseja deletar esse jogo?')]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar Jogos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="jogos form large-9 medium-8 columns content">
    <?= $this->Form->create($jogo) ?>
    <fieldset>
        <legend><?= __('Edit Jogo') ?></legend>
        <?php
            echo $this->Form->control('timeA');
            echo $this->Form->control('timeB');
            echo $this->Form->control('placarA');
            echo $this->Form->control('placarB');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
