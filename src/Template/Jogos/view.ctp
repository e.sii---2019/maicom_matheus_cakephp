<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $jogo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar Jogo'), ['action' => 'edit', $jogo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar Jogo'), ['action' => 'delete', $jogo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jogo->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar Jogos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo Jogo'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="jogos view large-9 medium-8 columns content">
    <h3><?= h($jogo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('TimeA') ?></th>
            <td><?= h($jogo->timeA) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('TimeB') ?></th>
            <td><?= h($jogo->timeB) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($jogo->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PlacarA') ?></th>
            <td><?= $this->Number->format($jogo->placarA) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PlacarB') ?></th>
            <td><?= $this->Number->format($jogo->placarB) ?></td>
        </tr>
    </table>
</div>
