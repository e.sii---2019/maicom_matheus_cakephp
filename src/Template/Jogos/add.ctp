<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $jogo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Jogos'), ['action' => 'index']) ?></li>
    </ul>
</nav>

    <?= $this->Form->create($jogo) ?>
    <fieldset>
        <legend><?= __('Add Jogo') ?></legend>
        

        <!-- <select>            
            <option>B</option>
            <option>A</option>
            <option>A</option>
            <option>A</option>
        </select> -->

        <?php
            echo $this->Form->control('timeA');
            echo $this->Form->control('timeB');
            echo $this->Form->control('placarA');
            echo $this->Form->control('placarB');
        ?>
    </fieldset>
    <?= $this->Form->button(__('ENVIAR')) ?>
    <?= $this->Form->end() ?>

