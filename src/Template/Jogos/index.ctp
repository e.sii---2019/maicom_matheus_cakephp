<?php

echo $this->Form->create(null, ['type' => 'get']);
	    
echo $this->Form->input('search', 
['class' => 'form-control', 'label' => false, 
'placeholder' => 'Digite aqui sua pesquisa', 
'value' => $this->request->query('search')]);

echo $this->Form->button('Pesquisar');
echo $this->Form->end();
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $jogos
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Novo Jogo'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="jogos index large-9 medium-8 columns content">
    <h3><?= __('Jogos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('timeA') ?></th>
                <th scope="col"><?= $this->Paginator->sort('timeB') ?></th>
                <th scope="col"><?= $this->Paginator->sort('placarA') ?></th>
                <th scope="col"><?= $this->Paginator->sort('placarB') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($jogos as $jogo): ?>
            <tr>
                <td><?= $this->Number->format($jogo->id) ?></td>
                <td><?= h($jogo->timeA) ?></td>
                <td><?= h($jogo->timeB) ?></td>
                <td><?= $this->Number->format($jogo->placarA) ?></td>
                <td><?= $this->Number->format($jogo->placarB) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $jogo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $jogo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $jogo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $jogo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
