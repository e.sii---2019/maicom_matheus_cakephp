##1.Instalação do projeto e dependências
Para acessar o projeto faça o download/clone da Master:

No caso de utilizar xampp, coloque o projeto dentro da pasta "htdocs"
Por exemplo: C:\xampp7\htdocs\pasta_seu_projeto

Após o clone, você pode utilizar o terminal da IDE com o diretório aberto ou gitBash para rodar comandos necessários:

Caso você esteja usando Xampp, configure o seu arquivo php.ini e adicione a extensão dinâmica:
extension="php_intl.dll"

Para atualizar o carregador do composer, acesse o diretório via gitbash ou terminal de sua preferência e rode o comando:
composer dumpautoload

Logo após, digite o comando a baixo, este comando vai baixar as dependências de arquivos e pastas com os arquivos do CakePHP.
composer install

Após estas configurações já é possível acessar o seu domínio local com o seu projeto, para isso(caso esteja utilizando XAMPP) 
http://127.0.0.1/pasta_seu_projeto

Nessa tela você terá acesso a informações sobre o seu projeto, como documentação oficial do CakePHP, configurações de segurança e também de acesso ao banco de dados.

##2.Configuração do Banco de Dados
As configurações de acesso ao banco de dados é definida no arquivo "app.php", o seu arquivo deve estar localizado em uma sequeência de diretórios semelhante a utilizada para desenvolver este projeto:
C:\xampp7\htdocs\pasta_seu_projeto\config

As configurações de Banco de Dados ficam dentro das definições "Datasources", no caso de estar desenvolvendo em ambiente local, os principais campos a serem alterados são:

Nome de usuário configurado no seu Banco de dados, no caso padrão do SQL, o usuário local é "root".
            'username' => 'root'

Senha do usuário padrão do SQL:
            'password' => ''

Nome do banco de dados utilizado no projeto
            'database' => 'cake'

Encoding utilizado para dar suporte para caracteres 'especiais' e acentuação.
            'encoding' => 'utf8mb4'

O próximo passo é criar um banco de dados com o nome 'cake', pode ser criado tanto via MySQL como pelo painel padrão do XAMPP:

Rota padrão do XAMPP:
http://localhost/phpmyadmin

Com o MySQL:
CREATE DATABASE IF NOT EXISTS `cake` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `cake`;

Após criado o banco de dados, atualize a página e veja se a conexão com o BD já está verde.

##3.Setando a base de dados
Execute as migrates: (Criar as tabelas)
bin/cake migrations migrate

Caso você queira testar a base de dados, você pode popular o banco com a seed:
bin/cake migrations seed


##3.Conteúdo Extra
Caso você queira ver como configrar o seu projeto CakePHP e desenvolver tabelas com o bake, deixamos estes vídeos a seguir:

Vídeo explicando instalação e criação de CRUD básica no CakePHP:
https://www.youtube.com/watch?v=rnb9q15TL_0

Vídeo explicando a configuração do CakePHP
https://youtu.be/WNDPBu8Uq84

