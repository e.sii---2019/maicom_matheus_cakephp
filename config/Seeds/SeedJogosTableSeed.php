<?php
use Migrations\AbstractSeed;

/**
 * SeedJogosTable seed.
 */
class SeedJogosTableSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'TimeA'    => 'França',
                'TimeB'    => 'Coréia do Sul',
                'PlacarA'    => 4,
                'PlacarB'    => 0,
            ], [
                'TimeA'    => 'Alemanha',
                'TimeB'    => 'China',
                'PlacarA'    => 1,
                'PlacarB'    => 0, 
            ], [
                'TimeA'    => 'Espanha',
                'TimeB'    => 'África do Sul',
                'PlacarA'    => 3,
                'PlacarB'    => 1, 
            ]

        ];

        $table = $this->table('jogos');
        $table->insert($data)->save();
    }
}
