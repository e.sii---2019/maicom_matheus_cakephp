<?php
use Migrations\AbstractSeed;

/**
 * SeedNacoesTables seed.
 */
class SeedNacoesTablesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'nacao'    => 'França',
                'grupo'    => 'A',
            ], [
                'nacao'    => 'Noruega',
                'grupo'    => 'A',
            ], [
                'nacao'    => 'Nigéria',
                'grupo'    => 'A',
            ], [
                'nacao'    => 'Coréia do Sul',
                'grupo'    => 'A',
            ],


            [
                'nacao'    => 'Alemanha',
                'grupo'    => 'B',
            ], [
                'nacao'    => 'Espanha',
                'grupo'    => 'B',
            ], [
                'nacao'    => 'China',
                'grupo'    => 'B',
            ],
            [
                'nacao'    => 'África do Sul',
                'grupo'    => 'B',
            ],


            [
                'nacao'    => 'Itália',
                'grupo'    => 'C',
            ], [
                'nacao'    => 'Austrália',
                'grupo'    => 'C',
            ], [
                'nacao'    => 'Brasil',
                'grupo'    => 'C',
            ],
            [
                'nacao'    => 'Jamaica',
                'grupo'    => 'C',
            ],


            [
                'nacao'    => 'Inglaterra',
                'grupo'    => 'D',
            ], [
                'nacao'    => 'Japão',
                'grupo'    => 'D',
            ], [
                'nacao'    => 'Argentina',
                'grupo'    => 'D',
            ],
            [
                'nacao'    => 'Escócia',
                'grupo'    => 'D',
            ],


            [
                'nacao'    => 'Holanda',
                'grupo'    => 'E',
            ], [
                'nacao'    => 'Canada',
                'grupo'    => 'E',
            ], [
                'nacao'    => 'Camarões',
                'grupo'    => 'E',
            ],
            [
                'nacao'    => 'Nova Zelândia',
                'grupo'    => 'E',
            ],


            [
                'nacao'    => 'Estados Unidos',
                'grupo'    => 'F',
            ], [
                'nacao'    => 'Suécia',
                'grupo'    => 'F',
            ], [
                'nacao'    => 'Chile',
                'grupo'    => 'F',
            ],
            [
                'nacao'    => 'Tailândia',
                'grupo'    => 'F',
            ]

        ];

        $table = $this->table('nacoes');
        $table->insert($data)->save();
    }
}
