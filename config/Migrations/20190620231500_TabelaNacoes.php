<?php
use Migrations\AbstractMigration;

class TabelaNacoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('nacoes');
        $table->addPrimaryKey('id', 'integer', [
            'autoIncrement' => true,
        ]);
        $table->addColumn('nacao', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ]);
        $table->addColumn('grupo', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 1,
        ]);
        $table->create();

    }
}
