<?php
use Migrations\AbstractMigration;

class TabelaJogos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('jogos');
        $table->addPrimaryKey('id', 'integer', [
            'autoIncrement' => true,
        ])
        ;
        $table->addColumn('timeA', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ]);
        $table->addColumn('timeB', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ]);
        $table->addColumn('placarA', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('placarB', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->create();
    }
}
